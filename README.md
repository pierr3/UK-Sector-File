# UK Sector File Project

The UK (EGTT and EGPX FIRs) Sector File project is an open source collaborative project to provide the highest quality sector file to VATSIM UK controllers.

A new sector file will be released in line with each airac cycle.

If you wish to contribute, [take a look at our contribution guide](https://gitlab.com/vatsim-uk/UK-Sector-File/blob/master/Contributing.md).